﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class GoldfeldQuandtTest
    {       

        public static double Run(double[,] y, double[,] x, int xIndex, int modelIndex)
        {
            if (xIndex >= x.GetLength(1) || xIndex < 0)
                throw new Exception("Нет параметра с таким индексом");

            // Выделяем один параметр
            var linesCount = x.GetLength(0); // Количество строк
            var columnCount = x.GetLength(1); // Количество строк

            // Сортируем матрицу X по выбраному параметру  Xi. Переставляя нужные Yi в матрице Y
            CustomShakerSort(y, x, xIndex);

            // Разделяем выборку на 3 части
            var partSize = (int)(x.GetLength(0) / 3);

            var yPart1 = new double[partSize, 1];
            var yPart2 = new double[partSize, 1];
            var yPart3 = new double[partSize, 1];

            var xPart1 = new double[partSize, columnCount];
            var xPart2 = new double[partSize, columnCount];
            var xPart3 = new double[partSize, columnCount];

            // Заполянем первую часть
            for (int i = 0; i < partSize; i++)
            {
                yPart1[i, 0] = y[i, 0];

                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    xPart1[i, columnIndex] = x[i, columnIndex];
                }

                //xPart1[i, 0] = temporaryX[i, 0];
            }

            // Заполянем вторую часть
            int lineIndex = 0;
            for (int i = partSize; i < partSize * 2; i++)
            {
                yPart2[lineIndex, 0] = y[i, 0];


                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    xPart2[lineIndex, columnIndex] = x[i, columnIndex];
                }

                lineIndex++;
            }

            // Заполянем третью часть
            lineIndex = 0;
            for (int i = partSize * 2; i < partSize * 3; i++)
            {
                yPart3[lineIndex, 0] = y[i, 0];

                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    xPart3[lineIndex, columnIndex] = x[i, columnIndex];
                }

                lineIndex++;
            }


            // Разделяем выборку 1 на две части. По первой считаем кооэфициенты b. По второй предсказываем значения y
            var cutArray = SplitArray(yPart1);
            var yPart1StudyPart = cutArray.Item1;
            var yPart1RealPart = cutArray.Item2;

            cutArray = SplitArray(xPart1);
            var xPart1StudyPart = cutArray.Item1;
            var xPart1PredictPart = cutArray.Item2;

            // Считаем кооэфициенты b для 1 выборки
            var bPart1 = MNK.Run(yPart1StudyPart, xPart1StudyPart);
            // Делаем прогноз по известным b
            var predictedYPart1 = Predict.Run(bPart1.ToArray(), xPart1PredictPart, modelIndex);

            var epsPart1 = DiffBetwenY(yPart1RealPart, predictedYPart1);


            // Разделяем выборку 3 на две части. По первой считаем кооэфициенты b. По второй предсказываем значения y
            cutArray = SplitArray(yPart3);
            var yPart3StudyPart = cutArray.Item1;
            var yPart3RealPart = cutArray.Item2;

            cutArray = SplitArray(xPart3);
            var xPart3StudyPart = cutArray.Item1;
            var xPart3PredictPart = cutArray.Item2;

            // Считаем кооэфициенты b для 1 выборки
            var bPart3 = MNK.Run(yPart3StudyPart, xPart3StudyPart);
            // Делаем прогноз по известным b
            var predictedYPart3 = Predict.Run(bPart3.ToArray(), xPart3PredictPart, modelIndex);

            var epsPart3 = DiffBetwenY(yPart3RealPart, predictedYPart3);


            // Вычисляем S1
            double S1 = 0;
            for (int i = 0; i < epsPart1.GetLength(0); i++)
            {
                S1 += Math.Pow(epsPart1[i], 2);
            }

            // Вычисляем S3
            double S3 = 0;
            for (int i = 0; i < epsPart3.GetLength(0); i++)
            {
                S3 += Math.Pow(epsPart3[i], 2);
            }


            // Вычисляем критерий Фишера
            double fisher = S3 / S1;

            return fisher;
            //return 0;
        }

        private static void CustomShakerSort(double[,] y, double[,] x, int xIndex)
        {
            var size = x.GetLength(0);
            var columnCount = x.GetLength(1);

            long j, k = size - 1;
            long lb = 1, ub = size - 1; // границы неотсортированной части массива
            double temp;

            do
            {
                // проход снизу вверх 
                for (j = ub; j > 0; j--)
                {
                    if (x[j - 1, xIndex] > x[j, xIndex])
                    {
                        // Перестановка в матрице X
                        /*temp = x[j - 1, 0];
                        x[j - 1, 0] = x[j, 0];
                        x[j, 0] = temp;*/

                        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                        {
                            temp = x[j - 1, columnIndex];
                            x[j - 1, columnIndex] = x[j, columnIndex];
                            x[j, columnIndex] = temp;
                        }

                        // Перестановка в матрице Y
                        temp = y[j - 1, 0];
                        y[j - 1, 0] = y[j, 0];
                        y[j, 0] = temp;

                        k = j;
                    }
                }

                lb = k + 1;

                // проход сверху вниз 
                for (j = 1; j <= ub; j++)
                {
                    if (x[j - 1, xIndex] > x[j, xIndex])
                    {
                        // Перестановка в матрице X
                        /*temp = x[j - 1, 0];
                        x[j - 1, 0] = x[j, 0];
                        x[j, 0] = temp;*/
                        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                        {
                            temp = x[j - 1, columnIndex];
                            x[j - 1, columnIndex] = x[j, columnIndex];
                            x[j, columnIndex] = temp;
                        }

                        // Перестановка в матрице Y
                        temp = y[j - 1, 0];
                        y[j - 1, 0] = y[j, 0];
                        y[j, 0] = temp;

                        k = j;
                    }
                }

                ub = k - 1;
            } while (lb < ub);
        }

        private static Tuple<double[,], double[,]> SplitArray(double[,] inputArray)
        {
            var columnCount = inputArray.GetLength(1);

            var part1Size = (int)(inputArray.GetLength(0) / 2);
            var part2Size = inputArray.GetLength(0) - part1Size;

            var array1 = new double[part1Size, columnCount];
            var array2 = new double[part2Size, columnCount];

            // Записываем первый массив
            for (int i = 0; i < part1Size; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    array1[i, j] = inputArray[i, j];
                }
            }

            // Записываем второй массив
            for (int i = 0; i < part2Size; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    array2[i, j] = inputArray[part1Size + i, j];
                }
            }

            return new Tuple<double[,], double[,]>(array1, array2);
        }

        private static double[] DiffBetwenY(double[,] realY, double[,] calcY)
        {
            var linesCount = realY.GetLength(0);

            var res = new double[linesCount];

            for (int i = 0; i < calcY.GetLength(0); i++)
            {
                res[i] = realY[i, 0] - calcY[i, 0];
            }

            return res;
        }
    }
}
