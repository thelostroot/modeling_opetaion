﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.CodeDom;
using System.Security.Cryptography;

namespace ModelingOperation
{
    class Program
    {
        const string PATH = @"C:\Users\dais\Desktop\МО\";
        const string FOLDER = "[2] GAZP = OIL_GAS";
        static void Main(string[] args)
        {
            // Загружаем данные
            var x = Helper.LoadFile($@"{PATH}\{FOLDER}\x.txt");
            var y = Helper.LoadFile($@"{PATH}\{FOLDER}\y.txt");

            // Добавляем колонку единиц к матрице X
            x = Helper.AddOneColumn(x);

            // Вычисляем коофициенты
            var b = MNK.Run(y, x);
            Console.WriteLine("b" + Environment.NewLine + b.ToMatrixString());

            int modelsCount = 4;
            for (int modelIndex = 1; modelIndex < modelsCount+1; modelIndex++)
            {
                Console.WriteLine($"Модель {modelIndex}:");

                // Предсказываем Y по X зная кооэфициенты b
                // Загружаем X по которым будем предсказывать Y
                var inputX = Helper.LoadFile($@"{PATH}\{FOLDER}\pred_x.txt");
                inputX = Helper.AddOneColumn(inputX);
                var predictedY = Predict.Run(b.ToArray(), inputX, modelIndex);
                System.IO.File.WriteAllText($@"{PATH}\{FOLDER}\model_{modelIndex}.txt", Helper.TransformPredictResult(predictedY) );

                // Загружаем реальные значения Y
                var realY = Helper.LoadFile($@"{PATH}\{FOLDER}\pred_y.txt");
                var R = CoefficientDetermination.Run(realY, predictedY);
                Console.WriteLine($"R: {R}");

                var DW = TestDurbinWatson.Run(realY, predictedY);
                Console.WriteLine($"DW: {DW}");

                var xForF = Helper.LoadFile($@"{PATH}\{FOLDER}\x.txt");
                var yForF = Helper.LoadFile($@"{PATH}\{FOLDER}\y.txt");
                var F = GoldfeldQuandtTest.Run(yForF, xForF, 1, modelIndex);
                Console.WriteLine($"F: {F}");

                var xForCorrelation = Helper.LoadFile($@"{PATH}\{FOLDER}\x.txt");
                var splitVariables = Helper.SplitByVariable(xForCorrelation);
                var r = Correlation.Run(splitVariables[0], splitVariables[1]);
                Console.WriteLine($"r: {r}");

                Console.WriteLine("\n\n");
            }            

            Console.ReadKey();
        } 
    }

}

