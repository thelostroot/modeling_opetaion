﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class MNK
    {
        public static Matrix<double> Run(double[,] y, double[,] x)
        {
            Matrix<double> xMatrix = DenseMatrix.OfArray(x);
            Matrix<double> yMatrix = DenseMatrix.OfArray(y);

            var transposeXmatrix = xMatrix.Transpose();

            var t = transposeXmatrix * xMatrix;

            t = t.Inverse();

            t = t * transposeXmatrix;
            t = t * yMatrix;

            return t;
        }
    }
}
