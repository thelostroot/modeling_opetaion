﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class Helper
    {
        public static double[,] LoadFile(string fileName)
        {
            const char SEPARATOR = ';';

            double[,] res;

            // Количество строк
            var lines = System.IO.File.ReadAllLines(fileName);

            // Количество колонок
            var columnCount = lines[0].Split(';').Length;

            // Создаем массив под данные
            res = new double[lines.Length, columnCount];

            for (int i = 0; i < lines.Length; i++)
            {
                var vars = lines[i].Split(SEPARATOR);

                for (int j = 0; j < vars.Length; j++)
                {
                    var value = vars[j].Replace(".", ",");
                    res[i, j] = Convert.ToDouble(value);
                }
            }

            return res;
        }

        // Доп. колонка едениц для X
        public static double[,] AddOneColumn(double[,] x)
        {
            // Новая матрица X на 1 колонку больше
            double[,] preparedX = new double[x.GetLength(0), x.GetLength(1) + 1];

            for (int i = 0; i < x.GetLength(0); i++)
            {
                preparedX[i, 0] = 1;

                for (int j = 0; j < x.GetLength(1); j++)
                {
                    preparedX[i, j + 1] = x[i, j];
                }
            }

            return preparedX;
        }

        // Разделяет массив X на массивы x1, x2 ... xn
        public static List<double[,]> SplitByVariable(double[,] x)
        {
            var columnsCount = x.GetLength(1);
            var linesCount = x.GetLength(0);

            // Создаем пустые массивы под каждую переменную
            var res = new List<double[,]>();
            for(int i=0;i< columnsCount; i++)
            {
                res.Add(new double[linesCount,1]);
            }

            // Заполняем массивы
            for (int i = 0; i < linesCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    var currentArray = res[j];
                    currentArray[i, 0] = x[i, j];
                }
            }

            return res;
        }

        public static string TransformPredictResult(double[,] result)
        {
            string predictedYString = "";
            for (int i = 0; i < result.GetLength(0); i++)
            {
                predictedYString += result[i, 0] + Environment.NewLine;
            }

            return predictedYString;
        }
    }
}
