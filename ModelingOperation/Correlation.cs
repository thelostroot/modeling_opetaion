﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class Correlation
    {
        public static double Run(double[,] x1, double[,] x2)
        {
            // Верхняя часть дроби
            double top = Covariance(x1, x2);

            // Нижняя часть дроби
            double bottom = Math.Sqrt(Dispersion(x1)) * Math.Sqrt(Dispersion(x2));

            return top / bottom;
        }

        private static double Covariance(double[,] x1, double[,] x2)
        {
            double res = 0;
            var linesCount = x1.GetLength(0);

            for (int i = 0; i < linesCount; i++)
            {
                res += x1[i, 0] * x2[i, 0];
            }

            double x1Average = Average(x1);
            double x2Average = Average(x2);

            return (res/linesCount) - (x1Average * x2Average);
        }

        private static double Average(double[,] x)
        {
            var linesCount = x.GetLength(0);

            double sum = 0;
            for (int i = 0; i < linesCount; i++)
            {
                sum += x[i, 0];
            }

            return sum / linesCount;
        }

        private static double Dispersion(double[,] x)
        {
            var average = Average(x);

            int n = x.GetLength(0);

            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += Math.Pow(x[i, 0], 2) - Math.Pow(average, 2);
            }

            return (1 / Convert.ToDouble(n) ) * sum;
        }
    }
}
