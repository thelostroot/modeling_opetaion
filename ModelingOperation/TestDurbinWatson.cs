﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class TestDurbinWatson
    {
        public static double Run(double[,] realY, double[,] predictedY)
        {
            int m = predictedY.GetLength(0);

            double fractionTop = 0;
            for (int i = 1; i < m; i++)
            {
                double eps = realY[i, 0] - predictedY[i, 0];
                double previousEps = realY[i - 1, 0] - predictedY[i - 1, 0];

                fractionTop += Math.Pow(eps - previousEps, 2);
            }

            double fractionBottom = 0;
            for (int i = 0; i < m; i++)
            {
                double eps = realY[i, 0] - predictedY[i, 0];
                fractionBottom += Math.Pow(eps, 2);
            }

            double DW = fractionTop / fractionBottom;
            return DW;
        }
    }
}
