﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class CoefficientDetermination
    {
        public static double Run(double[,] realY, double[,] predictedY)
        {
            // Вычисяем y с чертой
            double yStroke = 0;
            int m = predictedY.GetLength(0);
            for (int i = 0; i < m; i++)
                yStroke += predictedY[i, 0];

            yStroke = (1 / Convert.ToDouble(m) ) * yStroke;

            double fractionTop = 0;
            for (int i = 0; i < m; i++)
            {
                fractionTop += Math.Pow(realY[i, 0] - predictedY[i, 0], 2);
            }

            double fractionBottom = 0;
            for (int i = 0; i < m; i++)
            {
                fractionBottom += Math.Pow(realY[i, 0] - yStroke, 2);
            }

            double R = 1 - (fractionTop / fractionBottom);
            //R = Math.Pow(R , 2);

            return R;
        }
    }
}
