﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelingOperation
{
    public static class Predict
    {
        public static double[,] Run(double[,] b, double[,] x, int modelIndex)
        {
            double[,] predictedY = new double[x.GetLength(0), 1];
           

            // Идем по строкам
            for (int i = 0; i < x.GetLength(0); i++)
            {
                double y = 0;
                for (int j = 0; j < x.GetLength(1); j++)
                {                    
                    switch (modelIndex)
                    {
                        case 1:
                            y += b[j, 0] * x[i, j];
                            break;

                        case 2:
                            y += b[j, 0] * Math.Pow(x[i, j], 2);
                            break;                       

                        case 3:
                            y += b[j, 0] * Math.Pow(x[i, j], 3);
                            break;

                        case 4:
                            y += b[j, 0] * Math.Log(x[i, j]);
                            break;
                    }                    
                }

                /*
                switch(modelIndex)
                {
                    case 1:
                        y = b[0, 0] * x[i, 0] + b[1, 0] * x[i, 1] + b[2, 0] * x[i, 2] + b[3, 0] * x[i, 3];
                        break;
                    case 2:
                        y = b[0, 0] * x[i, 0] + b[1, 0] * x[i, 1] + b[2, 0] * x[i, 2] + b[3, 0] * x[i, 3];
                        break;
                    case 3:
                        y = b[0, 0] * x[i, 0] + b[1, 0] * x[i, 1] + b[2, 0] * x[i, 2] + b[3, 0] * x[i, 3];
                        break;
                    case 4:
                        y = b[0, 0] * x[i, 0] + b[1, 0] * x[i, 1] + b[2, 0] * x[i, 2] + b[3, 0] * x[i, 3];
                        break;
                }*/

                //Console.WriteLine(y);
                predictedY[i, 0] = y;
            }

            return predictedY;
        }
    }
}
